import {
   USER_LOGIN_SUCCESS,
   USER_SIGN_IN_SUCCESS
} from '../actions/constants';

const initialState = null;

export default (state = initialState, action) => {
   switch (action.type) {

      case USER_LOGIN_SUCCESS:
         return action.payload;

      case USER_SIGN_IN_SUCCESS:
         return action.payload;

      default:
         return state;
   }
}