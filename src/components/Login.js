import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { login } from '../actions/authActions';
import { withRouter } from 'react-router-dom';

class Login extends Component {
   redirectTo = () => {
      this.props.history.push('/protected');
   };
   render() {
      return (
         <div className="login">
            <h1>Login page</h1>
            <div className="login-form">
               <Formik
                  initialValues={{ email: '', password: '' }}
                  validate={values => {
                     let errors = {};
                     if (!values.email) {
                        errors.email = 'Required';
                     } else if (
                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                     ) {
                        errors.email = 'Invalid email address';
                     }
                     return errors;
                  }}
                  onSubmit={(values, { setSubmitting }) => {
                     this.props.login(values, this.redirectTo);
                     setSubmitting(false);
                  }}
               >
                  {({ isSubmitting }) => (
                     <Form>
                        <Field type="email" name="email" placeholder="email" />
                        <ErrorMessage name="email" component="div" />
                        <Field type="password" name="password" placeholder="password" />
                        <ErrorMessage name="password" component="div" />
                        <button type="submit" disabled={isSubmitting}>
                           Submit
                        </button>
                     </Form>
                  )}
               </Formik>
            </div>
         </div>
      )
   }
}

const mapDispatchToProps = dispatch => bindActionCreators({
   login
}, dispatch);
export default withRouter(connect(null, mapDispatchToProps)(Login));