import React, { Component } from 'react';

class NotFound extends Component {

   render() {
      return (
         <div className="not-found">
            <div className="text">
               page not found or in progress..
            </div>
         </div>
      )
   }
}

export default NotFound;