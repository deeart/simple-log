import React, { Component } from 'react';
import { connect } from 'react-redux';
import authHOC from './authHOC';

class ProtectedPage extends Component {

   render() {
      return (
         <div className="protected">
            <h1>Protected page</h1>
         </div>
      )
   }
}
const mapStateToProps = state => ({
   user: state.userReducer,
});
export default authHOC(connect(mapStateToProps, null)(ProtectedPage));