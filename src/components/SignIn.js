import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import {bindActionCreators} from "redux";
import {signIn} from "../actions/authActions";
import {connect} from "react-redux";
import { withRouter } from 'react-router-dom';

class SignIn extends Component {
   redirectTo = () => {
      this.props.history.push('/protected');
   };
   render() {
      return (
         <div className="signIn">
            <h1>Sign-in page</h1>
            <div className="sign-in-form">
               <Formik
                  initialValues={{ email: '', password: '' }}
                  validate={values => {
                     let errors = {};
                     if (!values.email) {
                        errors.email = 'Required';
                     } else if (
                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                     ) {
                        errors.email = 'Invalid email address';
                     }
                     if (!values.firstName) {
                        errors.firstName = 'Required';
                     }
                     if (!values.lastName) {
                        errors.lastName = 'Required';
                     }
                     console.log(errors);
                     return errors;
                  }}
                  onSubmit={(values, { setSubmitting }) => {
                     this.props.signIn(values, this.redirectTo);
                     setSubmitting(false);
                  }}
               >
                  {({ isSubmitting }) => (
                     <Form>
                        <Field type="text" name="firstName" placeholder="first name" />
                        <ErrorMessage name="firstName" component="div" />
                        <Field type="text" name="lastName" placeholder="last name" />
                        <ErrorMessage name="lastName" component="div" />
                        <Field type="email" name="email" placeholder="email" />
                        <ErrorMessage name="email" component="div" />
                        <Field type="password" name="password" placeholder="password" />
                        <ErrorMessage name="password" component="div" />
                        <Field type="text" name="invite" placeholder="invite" />
                        <ErrorMessage name="invite" component="div" />
                        <button type="submit" disabled={isSubmitting}>
                           Submit
                        </button>
                     </Form>
                  )}
               </Formik>
            </div>
         </div>
      )
   }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => bindActionCreators({
   signIn
}, dispatch);
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignIn));