import React from 'react';
import { NavLink } from 'react-router-dom';

export default class Header extends React.Component {
   render() {
      return (
         <header>
            <div className="container">
               <div className="buttons">
                  <NavLink activeClassName="active" to="/login">
                     <button className="login">log in</button>
                  </NavLink>
                  <NavLink activeClassName="active" to="/sign-in">
                     <button className="singIn">sign in</button>
                  </NavLink>
               </div>
            </div>
         </header>
      )
   }
}