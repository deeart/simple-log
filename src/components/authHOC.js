import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from "react-router-dom";

export default function (ComposedComponent) {
   class Authenticate extends React.Component {

      componentDidMount() {
         this._checkAndRedirect();
      }

      componentDidUpdate() {
         this._checkAndRedirect();
      }

      _checkAndRedirect() {
         const { user } = this.props;

         if (!user) {
            this.props.history.push('/login');
         }
      }

      render() {
         return (
            <div>
               { this.props.user ? <ComposedComponent {...this.props} /> : null }
            </div>
         );
      }
   }

   const mapStateToProps = (state) => {
      return {
         user: state.userReducer
      };
   };

   return withRouter(connect(
      mapStateToProps,
      null
   )(Authenticate));
}