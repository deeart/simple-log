import React, { Suspense, Component } from 'react';
import {Route, Switch} from 'react-router';
import {BrowserRouter, Redirect} from 'react-router-dom';
import Header from './components/Header';
import NotFound from './components/NotFound';
import './App.scss';

const Login = React.lazy(() => import('./components/Login'));
const SignIn = React.lazy(() => import('./components/SignIn'));
const ProtectedPage = React.lazy(() => import('./components/protectedPage'));

class App extends Component {
  render() {
    return (
      <div className="App">
         <BrowserRouter>
            <div className="container">
                  <Header />
                  <Switch>
                     <Route path="/login" render={() => (
                        <Suspense fallback={<div>Loading...</div>}>
                           <Login />
                        </Suspense>
                     )} />
                     <Route path="/sign-in" render={() => (
                        <Suspense fallback={<div>Loading...</div>}>
                           <SignIn />
                        </Suspense>
                     )} />
                     <Route path="/protected" render={() => (
                        <Suspense fallback={<div>Loading...</div>}>
                           <ProtectedPage />
                        </Suspense>
                     )} />
                     <Route path="*" render={() => (
                        <Suspense fallback={<div>Loading...</div>}>
                           <NotFound />
                        </Suspense>
                     )} />
                  </Switch>
               </div>
         </BrowserRouter>
      </div>
    );
  }
}

export default App;
