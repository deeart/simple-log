import * as constants from './constants';
import axios from 'axios';

export const login = (val, callback) => {
   return dispatch => {
      axios.post(
         constants.API_LOGIN,
         val
      ).then(res => {
         dispatch({type: constants.USER_LOGIN_SUCCESS, payload: res.data});
         if (callback) {
            callback();
         }
      }).catch(err => console.log(err));
   }
};

export const signIn = (val, callback) => {
   return dispatch => {
      axios.post(
         constants.API_SIGN_IN,
         val
      ).then(res => {
         dispatch({type: constants.USER_SIGN_IN_SUCCESS, payload: res.data})
         if (callback) {
            callback();
         }
      }).catch(err => console.log(err));
   }
};